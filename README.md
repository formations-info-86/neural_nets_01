# Réseaux de neurones


Les fiches de cours sont disponibles avec les documents de la formation NSI 2022 (janvier 2022).

1. Expérimentation avec [Playground Tensorflow](playground_tensorflow.md)
2. Écriture d'un perceptron, algo de rétropropagation du gradient, et tests sur les bases iris et MNIST784 : [Programmation du perceptron](programmation_perceptron.md)
3. Utilisation de bibliothèques spécialisées. Ce travail est profitable si on a programmé le perceptron, car les paramètres à régler prennent
  plus de sens.
    - [Utilisation de scikit-learn](utilisation_sklearn.md)
    - [Utilisation de keras / Perceptron multicouches](utilisation_keras_mlp.md)
    - [Utilisation de keras / réseau convolutif](utilisation_keras_convolutif.md)

Il est (...probablement...) possible de réaliser ce travail dans un notebook. En l'absence d'autre infrastructure, *Google* [colab](https://colab.research.google.com) peut s'avérer très précieux. Pour démarrer avec l'outil, importez le notebook de ce dépôt nommé [`utilisation_colab.ipynb`](utilisation_colab.ipynb) qui indique comment accéder à des données externes (et nous en avons besoin pour ce travail)

Image projet : *Fakurian Design*
