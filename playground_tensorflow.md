# Utilisation de playground Tensorflow

L'outil de démo est disponible dans une page Web : [playground tensorflow](https://playground.tensorflow.org).

Les manipulations proposées sont issues de l'ouvrage *Hands on machine learning* d'Aurélien Géron.

1. en laissant toutes les options par défaut, cliquez sur Run, puis reset
   plusieurs fois pour voir comment le réseau converge vers une solution
   satisfaisante.
2. remplacez la fonction d'activation tangente hyperbolique par ReLU, et
   constatez que les frontières sont maintenant plus anguleuses
3. créez un réseau d'une seule couche cachée contenant 3 neurones. Testez
   plusieurs fois la convergence. Dans certains cas, le réseau reste coincé sur
   un minimum local non satisfaisant (mais ça reste assez rare)
4. en diminuant le nombre de neurones, le réseau ne parvient plus jamais à
   converger vers une solution satisfaisante (sous apprentissage)
5. en augmentant le nombre de neurones, il y parvient toujours, les minima
   locaux sont presque aussi satisfaisants que le minimum global

