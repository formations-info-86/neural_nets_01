# Programmation d'un perceptron multi-couche et de la rétropropagation du gradient

## Écriture du perceptron multi-couche

Le fichier [`mlp_skel_01.py`](mlp_skel_01.py) est un squelette contenant les classes Layer et Network permettant
de réaliser un perceptron multi-couches, et la méthode de rétropropagation du gradient.

Toutes les méthodes sont présentes et documentées, mais pas détaillées. C'est le travail à réaliser.
La façon de mener les calculs, ainsi que les notations utilisées sont celles des fiches de cours.

En cas de difficulté, nous disposons d'autres squelettes, plus complets, qui ne nécessitent pas 
d'écrire toutes les méthodes. 

Lorsque votre code est prêt (ça peut prendre pas mal de temps...), vous pouvez le tester en lisant
la section suivante.

Nous proposons de procéder dans cet ordre :

- Prendre connaissance (de manière approfondi) du contenu du fichier
- Lire `Layer.__init__`
- Écrire la méthode `Layer.forward`
- Lire `Network.__init__`
- Lire `Network.ajoute_couche`
- Écrire la méthode `Network.forward`
- Tester une passe avant (voir plus loin les donnés de test fournies). Pour cela :
  - faire un fichier de test à part, qui importe `mlp`
  - instancier un réseau
  - lui ajouter les couches (identiques à celles de la partie Test plus loin)
  - injecter des entrées, et vérifier le calcul
  - tester l'affichage `print(net)` où `net` est une instance de `Network`
- Écrire la fonction `Layer.back` (demandez la solution si vous voulez)
- Écrire la fonction `Network.back` (id.)
- Écrire la fonction `Layer.learn`
- Écrire la fonction `Network.learn`
- Écrire la fonction `Network.performance_classifieur`

Éléments qui peuvent être utiles :

- `np.concatenate` pour concaténer des vecteurs (par exemple pour ajouter le neurone de biais à 1 aux entrées)
- le produit matriciel est noté `@` dans `numpy`
- `np.reshape` pour modifier la taille d'une matrice
- `np.ravel` pour transformer uen matrice en vecteur
- ...


## Test du code du perceptron multi-couche

Il est assez facile de faire une erreur de calcul... aussi, nous proposons d'écrire un programme
de test assez simple, avec peu de neurones. L'idée est de partir d'une configuration fixée, et
de comparer les résultats avec ceux attendus, qui sont donnés dans le fichier
[`test_01_output.txt`](test_01_output.txt). 

Vous devrez vérifier que vous avez les bonnes valeurs **partout**, c'est à dire
que le réseau calcule correctement sa sortie (`forward`), qu'il évalue correctement les gradients
(`back`), et met à jour correctement les poids (`learn`).

Pour réaliser ce test, configurez votre code pour utiliser un réseau à deux entrées, 
une première couche de 2 neurones, et une couche de sortie d'un seul neurone.
La base d'apprentissage à utiliser est la suivante (c'est un XOR) :

```
0, 0 -> 0
0, 1 -> 1
1, 0 -> 1
1, 1 -> 0
```

Les poids ne seront pas initialisés aléatoirement (sinon, impossible de comparer avec le résultat attendu).
Il faudra donc les forcer aux matrices suivantes :

```
poids de la première couche :

0.1 -0.1 0.1
0.2 0.2 -0.1

poids de la couche de sortie :

-0.1 0.1 0.1
```

Le facteur d'apprentissage sera fixe et égal à 10.

Le fichier [`test_01_output.txt`](test_01_output.txt) :

- indique la sortie du réseau pour chacune des 4 entrées, avant apprentissage
- détaille les calculs pour une passe d'apprentissage (chaque exemple est
  présenté une fois, et on ajuste les poids après chacun des 4 exemples). Les détail montrent :
  - les dérivées du coût par rapport aux sorties de chaque neurone de chaque couche (valeur rétropropagée)
  - l'état du réseau (poids, entrées, sorties)
  - le gradient du coût par rapport aux poids, calculé pour chaque couche (on a
    autant de valeurs qu'il y a de paramètres dans le réseau)
  - l'état du réseau après l'application du gradient

![Données disponibles au passage du premier exemple](imgs/exploitation_example_backpropagation.png)

Rappel : pour chacun des 4 exemples, on utilise le gradient stochastique :
 - présentation d'un exemple
 - mise à jour des poids

À la fin de l'epoch 1 (on a passé les 4 exemples), la nouvelle
erreur est donnée. 
On indique aussi la réponse du réseau pour chacune des 4 entrées

Vous devez obtenir **exactement** les mêmes résultats que ceux donnés dans le fichier.

### Quelques expériences

Une passe ne suffit pas pour que le XOR soit appris.
Avec un facteur d'apprentissage de 10, réalisez 500 passes et
tracez l'évolution de l'erreur moyenne sur la base d'apprentissage, au fil
des 500 itérations.

Faites de même avec un facteur d'apprentissage de 50.

Puis à nouveau avec un facteur d'apprentissage de 1. Éventuellement, réalisez 1000 itérations
plutôt que 500.

## Base d'Iris

Nous proposons de tester le réseau sur la base de données iris.
Cette base contient pour chaque échantillon, 4 entrées caractéristiques, et 
le type d'Iris (3 espèces différentes).
Le réseau à utiliser sera un classifieur, contenant 4 entrées, une couche cachée de 6 neurones
et une couche de sortie de 3 neurones (puisqu'il y a 3 espèces d'Iris).

La base d'apprentissage sera constituée de 100 exemples, et la base de test des 50 exemples restant.
Un taux d'apprentissage fixe de 0.3 peut être utilisé, et il n'est pas nécessaire de faire des 
mini-batch (le gradient stochastique sur chaque exemple fonctionne).

Le script [`download_iris.sh`](download_iris.sh) indique comment télécharger les données (fichier .csv).

Pensez à normaliser les données d'entrée (chaque feature doit être un nombre entre 0 et 1). Il est intéressant de voir la différence de comportement de la phase d'apprentissage selon que les données sont normalisées ou non (la décroissance de l'erreur est moins accidentée avec des données normalisées).

Une cinquantaine de passes (*epochs*) permet d'obtenir une classification des éléments de la base de test 
avec une fiabilité supérieure à 95 %. 

Ajoutez une fonction capable de vous donner la [matrice de confusion](https://fr.wikipedia.org/wiki/Matrice_de_confusion)
donnant les performances de votre système.

## Base MNIST

La base mnist contient 70 000 imagettes 28x28 représentant des chiffres.
Un réseau tel que celui que vous avez programmé peut atteindre de bons résultats avec les paramètres
suivants : 

- 784 entrées (il faudra normaliser les valeurs des pixels)
- une couche cachée de 30 neurones (on voit parfois des tests avec 200 neurones, vous pouvez essayer)
- une couche de sortie de 10 neurones
- une base d'apprentissage de 60 000 exemples
- un gradient stochastique avec mini-batch de 10 exemples (ce point est important)
- un facteur d'apprentissage fixe égal à 0.3

Le script [`download_mnist.sh`](download_mnist.sh) indique comment télécharger les données (fichier .zip, puis extraction du csv).

Voici le comportement du réseau en cours d'apprentissage : 

```plain
Reading Data
Data read
Avant apprentissage
    Train : 8.82 %
    Test  : 8.69 %
    Erreur Train : 2.13814


000 : alpha=3, batch_size=10, Error : 0.07555
    Train : 90.36 %
    Test  : 90.88 %
    Time  : 35.9

001 : alpha=3, batch_size=10, Error : 0.05867
    Train : 92.83 %
    Test  : 92.39 %
    Time  : 64.4

002 : alpha=3, batch_size=10, Error : 0.05059
    Train : 93.88 %
    Test  : 93.35 %
    Time  : 92.2

003 : alpha=3, batch_size=10, Error : 0.04599
    Train : 94.54 %
    Test  : 93.82 %
    Time  : 120.2

004 : alpha=3, batch_size=10, Error : 0.04359
    Train : 94.80 %
    Test  : 94.16 %
    Time  : 148.8

005 : alpha=3, batch_size=10, Error : 0.04120
    Train : 95.08 %
    Test  : 94.45 %
    Time  : 176.2

006 : alpha=3, batch_size=10, Error : 0.04089
    Train : 95.19 %
    Test  : 94.56 %
    Time  : 205.8
```

Voici le type de décroissance d'erreur obtenue pour un perceptron contenant 30 neurones dans la couche cachée, et 10 en sortie, pour la classification.

![](imgs/decroissance_erreur_784_30_10.png)

### Quelques tests

Essayez de faire varier la taille du mini-batch :

- 60 000 pour avoir avoir comme coût l'erreur sur tout l'ensemble d'apprentissage
- 1 pour suivre la direction du gradient à chaque exemple

