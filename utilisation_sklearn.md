# Utilisation de `scikit-learn`

`scikit-learn` contient de nombreux algorithmes de machine learning.
N'étant pas spécialisé dans les réseaux de neurones, il ne propose pas tous les types de réseaux, mais 
il est possible de tester un classifieur à base de perceptron multicouches, du même type que celui que nous avons programmé.
Nous aurons en plus la possibilité de faire varier quelques paramètres.

Voici un exemple d'utilisation :

```python
from sklearn.neural_network import MLPClassifier
from sklearn import metrics

# Ici, YtrainClass est le numéro de la classe : [0, 2, 1, 1, 2, 0...]
# Choix de la fonction d'activation, de l'algorithme d'apprentissage, de la taille du mini batch 
# du nombre de passes, et des couches cachées (ici une seule avec 6 neurones)
model = MLPClassifier(activation='logistic', solver='adam', max_iter=500, hidden_layer_sizes=(6,))
# Entraînement du réseau
model.fit(Xtrain, YtrainClass)
# Performance du réseau 
print(model.score(Xtest, YtestClass))
predictionClass = model.predict(Xtest)
# Mesures des performances
print(metrics.confusion_matrix(YtestClass, predictionClass))
print(metrics.accuracy_score(YtestClass, predictionClass))
```

La documentation est disponible sur la page [MLPClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html), 
on y trouve les différentes options pour la fonction d'activation, l'algorithme d'optimisation etc.

D'autres information sont disponibles ici : [Neural Networks Models (supervised)](https://scikit-learn.org/stable/modules/neural_networks_supervised.html)


Utilisez `scikit-learn` à la place de votre module `mlp` pour tester l'apprentissage sur la base `iris`.
