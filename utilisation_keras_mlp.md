# Utilisation de `keras`

Les cas d'utilisation sont trop nombreux pour que nous puissions donner une description complète.
La documentation [en ligne](https://keras.io).

Voici néanmoins quelques lignes de code pour démarrer, qui reproduisent le perceptron multicouches que 
nous avons programmé *from scratch* pour reconnaître les iris.

```python
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt

# Utilisation de Keras
model = Sequential()
# Ajout d'une couche de 6 neurones, avec 4 entrées
model.add(Dense(units=6,input_shape=(4,) ,activation="sigmoid"))
# Ajout de la couche de sortie, à 10 neurones
model.add(Dense(units=3,activation="sigmoid"))
# Choix de la fonction coût, de l'algorihtme d'apprentissage et de la mesure pour les performances
model.compile(loss="mean_squared_error", optimizer="sgd", metrics=["accuracy"])
# Lancement de l'apprentissage, nombre de passes, et taille du mini batch
history = model.fit(XTrain, yTrain, epochs=500, batch_size=None)
# Évaluation des résultats
prediction = model.predict(xTest)
score = model.evaluate(XTest,yTest)
# Courbe d'apprentissage
plt.plot(history.history["accuracy"])
plt.show()
```

Avec `keras`, nous avons beaucoup d'outils à disposition. En particulier, nous pouvons utiliser la fonction `ReLU`
(voir <https://keras.io/api/layers/>) dans la couche cachée, et `softmax` plutôt qu'une sigmoïde dans la couche de sortie.

L'algorithme d'optimisation `adam` est généralement plus performante que la descente de gradient stochastique `sgd`.
Enfin, pour un problème de classification, la fonction de coût «entropie croisée»
(cherchez la ici : <https://keras.io/api/losses/>) est plus pertinente que l'erreur quadratique.

Modifiez le réseau et comparez ses performances, toujours sur la base d'Iris.

## Base MNIST

Vous pouvez réaliser la même comparaison sur la base MNIST (avec 20 passes, ce qui est assez long, on passe d'une précision de 89% à 95% environ).