# Réseau convolutif avec Keras

Ce document est la suite de : [Utilisation de keras : perceptron multicouches](utilisation_keras_mlp.md).

On propose ici des manipulation qui permettent d'utiliser et de visualiser les couches convolutives d'une réseau de neurones
utilisé sur la base de chiffres MNIST.

Il est possible de paramétrer un réseau convolutif avec `keras`. 
Pour cela, il faudra tout d'abord modifier les images en tenseurs avec `reshape` :

```python
>>> Xtrain.shape
   (60000, 784)
>>> ... # utiliser reshape
>>> Xtrain.shape
   (60000, 28, 28, 1)
```

On pourra utiliser une couche de convolution avec 32 noyaux 5x5 (voir
[Conv2D](https://keras.io/api/layers/convolution_layers/convolution2d/)), suivie
d'une couche de *pooling*, qui agrège les données d'un carré 2x2 (voir
[MaxPooling2D](https://keras.io/api/layers/pooling_layers/max_pooling2d/)).
Généralement, les données sont ensuite remises sous forme d'un vecteur (perte de
l'image 2D) avec une couche
[Flatten](https://keras.io/api/layers/reshaping_layers/flatten/), puis on peut
placer la couche de sortie (`Dense`), de 10 neurones.

Les résultats obtenus avec ce type de réseau sur la base MNIST de chiffres, tous
paramètres égaux par ailleurs, dépassent généralement 98%.

On peut de plus mettre plusieurs couches convolutives et plusieurs couches denses.

## Visualisation du réseau convolutif

Accéder aux calculs faits par `keras` est possible, mais pas toujours très facile. On manipule parfois des listes, parfois des tableaux `numpy`, 
parfois des tenseurs `Tensorflow`, parfois des tenseurs `KerasTensor`...
Voici quelques éléments pour essayer de s'y retrouver.

On suppose que la première couche est formée de 32 noyaux 5x5 et qui opèrent du des images 28x28, et que
l'activation est une fonction `relu` et  le `padding` vaut `valid` (ainsi, le résultat de la convolution avec 
un des noyaux est une image 24x24, car on ne s'approche pas trop des bords).

## Accès aux poids et biais des noyaux
On peut accéder aux poids du modèle avec : `model.get_weight()`.
La fonction renvoie une liste :
- le premier élément contient les poids des 32 noyaux
- le second élément contient les biais des 32 noyaux
- la suite dépend des autres couches mises dans le réseau

Récupérons les poids et les biais : 

```python
poids_conv = model.get_weights()[0] # array (5, 5, 1, 32)
bias_conv = model.get_weights()[1]  # array (32,)
```

Les poids forment donc un tenseur de taille (5, 5, 1, 32) : 5 x 5 est la
dimension du noyau, 1 est le nombre de plans (ici des images en niveau de gris),
et il a 32 noyaux.

On peut visualiser les matrices 5x5 formées par les poids, qui décrivent chacun des 32 motifs simples 
détectés par le réseau convolutif :

```python
import matplotlib.pyplot as plt
for k in range(poids_conv.shape[-1]):
    plt.subplot(6, 6, k + 1)
    plt.axis('off')
    plt.imshow(poids_conv[:,:,:,k], interpolation="nearest",cmap="gray")
plt.show()
```

![](imgs/32_kernels.png)

## Calcul réalisé par le réseau sur une entrée particulière

On commence par récupérer une image de MNIST, par exemple l'image 1 (la deuxième) :

```python
img = Xtrain[1:2, :, :, :]
```

Attention, `img` a pour taille `(1, 28, 28, 1)`  : 1 seul exemple (`1:2`), taille 28x28, un seul plan (`:` final qui prend tous les plans, 
mais on n'en avait qu'un seul dès le début).

Visualisons l'image : 

```python
plt.imshow(img.reshape((28, 28)))
plt.show()
```

![](imgs/image_0.png)

Il est possible de récupérer les sorties intermédiaires du réseau convolutif.
On pourra pour cela utiliser une des méthodes commentées ici [Keras : How to get the output of each layer](https://stackoverflow.com/questions/41711190/keras-how-to-get-the-output-of-each-layer).

Dans la suite, nous choisissons l'option de construire un modèle intermédiaire :

```python
from keras.models import Model

layer_model = Model(inputs=model.input, outputs=model.get_layer(index=0).output)

```
Ce modèle fictif a les entrées de notre modèle complet et a comme sorties, la sortie de la couche de convolution de notre modèle.

On utilise alors ce modèle très simplement :

```python
output = layer_model.predict(img)
```

La sortie `output` est un tableau de dimension `(1, 24, 24, 32)` (1 seul exemple, des images 24x24, et une pour chacun des 32 noyaux).

Il est alors possible d'afficher côte à côte le noyau utilisé, et le résultat de la convolution : 

```python
for k in range(poids_conv.shape[-1]):
    plt.subplot(6, 12, 2*k +1)
    plt.axis('off')
    plt.imshow(poids_conv[:,:,:,k], interpolation="nearest",cmap="gray")
    plt.subplot(6, 12, 2*k + 2)
    plt.axis('off')
    plt.imshow(output[0, :, :, k], interpolation="nearest",cmap="gray")
plt.show()
```

![](imgs/32_kernels_result.png)

## «Vérification du calcul»

Ce n'est pas très pratique sur des images de cette taille, mais il est possible de vérifier les calculs (attention à penser au biais).
Les valeurs strictement négatives après la convolution sont annulées par la fontion d'activation `ReLU` dans notre exemple.

On obtient par exemple avec le kernel numéro 5 :

```python
np.set_printoptions(precision=2, suppress=True, linewidth=180)
nkernel = 5
print("Image")
print(img[0, :, :, 0])
print("Kernel : ")
print(poids_conv[:, :, 0, nkernel])
print("Biais : ", bias_conv[nkernel])
print("Sortie :")
print(output[0, : , :,  nkernel])
```

Ce qui donne : 

```plain
Image
[[0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.2  0.62 0.99 0.62 0.2  0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.19 0.93 0.99 0.99 0.99 0.93 0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.21 0.89 0.99 0.99 0.94 0.91 0.99 0.22 0.02 0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.04 0.24 0.88 0.99 0.99 0.99 0.79 0.33 0.99 0.99 0.48 0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.64 0.99 0.99 0.99 0.99 0.99 0.99 0.38 0.74 0.99 0.65 0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.2  0.93 0.99 0.99 0.75 0.45 0.99 0.89 0.18 0.31 1.   0.66 0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.19 0.93 0.99 0.99 0.7  0.05 0.29 0.47 0.08 0.   0.   0.99 0.95 0.2  0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.15 0.65 0.99 0.91 0.82 0.33 0.   0.   0.   0.   0.   0.   0.99 0.99 0.65 0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.03 0.7  0.99 0.94 0.28 0.07 0.11 0.   0.   0.   0.   0.   0.   0.99 0.99 0.76 0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.22 0.99 0.99 0.25 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.99 0.99 0.76 0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.78 0.99 0.75 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   1.   0.99 0.77 0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.3  0.96 0.99 0.44 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.99 0.99 0.58 0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.33 0.99 0.9  0.1  0.   0.   0.   0.   0.   0.   0.   0.   0.03 0.53 0.99 0.73 0.05 0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.33 0.99 0.87 0.   0.   0.   0.   0.   0.   0.   0.   0.03 0.51 0.99 0.88 0.28 0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.33 0.99 0.57 0.   0.   0.   0.   0.   0.   0.   0.19 0.65 0.99 0.68 0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.34 0.99 0.88 0.   0.   0.   0.   0.   0.   0.45 0.93 0.99 0.64 0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.33 0.99 0.98 0.57 0.19 0.11 0.33 0.7  0.88 0.99 0.87 0.65 0.22 0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.33 0.99 0.99 0.99 0.9  0.84 0.99 0.99 0.99 0.77 0.51 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.11 0.78 0.99 0.99 0.99 0.99 0.99 0.91 0.57 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.1  0.5  0.99 0.99 0.99 0.55 0.15 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]]

Kernel : 
[[-0.04 -0.28 -0.63 -0.61  0.51]
 [ 0.03 -0.04 -0.42 -0.41 -0.26]
 [ 0.07  0.22  0.15 -0.29 -0.56]
 [ 0.18  0.35  0.2  -0.35 -0.45]
 [ 0.08  0.32  0.14 -0.04 -0.14]]

Biais :  -0.0068008676

Sortie :
[[0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.02 0.29 0.44 0.3  0.11 0.01 0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.5  1.   0.95 0.55 0.1  0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.3  1.12 1.43 1.   0.33 0.02 0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.23 1.13 1.28 0.78 0.27 0.04 0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   1.02 1.21 0.65 0.13 0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.35 1.36 0.87 0.2  0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   1.   1.01 0.31 0.01 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.32 1.06 0.56 0.08 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.01 1.23 0.81 0.19 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   1.23 0.96 0.24 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.86 0.9  0.25 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.49 0.71 0.22 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.22 0.42 0.14 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.1  0.03 0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.04 0.04 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.34 0.08 0.   0.   0.   0.   0.   0.   0.   0.02 0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.02 0.75 0.38 0.   0.   0.   0.   0.   0.   0.06 0.37 0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.87 0.6  0.06 0.   0.   0.   0.   0.   0.27 0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.4  0.31 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.02 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.14 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.05 0.3  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]
 [0.   0.   0.   0.04 0.19 0.13 0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.   0.  ]]
 ```
