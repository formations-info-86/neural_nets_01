from typing import Tuple, Callable, List
import numpy as np


# ==============================  Utilitaires  =====================================
def sigmoid(x):
    """ Fonction sigmoïde vectorielle """
    return 1.0 / (1.0 + np.exp(-x))

def sigmoid_prime(x):
    """ Dérivée de la fonction sigmoïde vectorielle """
    return sigmoid(x) * (1.0 - sigmoid(x))

# ============================== Couche d'un réseau de neurones =====================================

class Layer:
    """
    Représente une couche d'un réseau de neurones, contient les poids, la fonction d'activation,
    sa dérivée, et des caches pour les entrées, les entrées pondérées, les sorties.
    Cette classe est directement instanciée par un objet Network.
    """
    def __init__(self, nb_neurones: int, nb_entrees: int,
                       fn_activation: Callable,
                       fn_activation_prime: Callable):
        """
        :param nb_neurones: nombre de neurones de la couche
        :param nb_entrees: nombre d'entrées de la couche (sans le biais)
        :param fn_activation: fonction **vectorielle** d'activation
        :param fn_activation_prime: dérivée **vectorielle** de la fonction d'activation

        Paramètres internes :
        self._z : entrées de la couche **avec** le neurone de biais
        self._p : entrées pondérées de chaque neurone
        self._zp : sorties de la couche
        self._poids : matrice des poids (y compris de le biais) de nb_neurones lignes
                      et nb_entrees + 1 colonnes

        Les poids sont initialisées à de «petites» valeurs aléatoires
        """
        self._nb_neurones = nb_neurones
        self._nb_entrees = nb_entrees
        self._activation = fn_activation
        self._activation_prime = fn_activation_prime
        self._z = None
        self._zp = None
        self._p = None
        # Autant de lignes qu'il y a de neurones dans la couche
        # Artant de colonnes qu'il y a d'entrées (biais compris)
        self._poids = np.random.randn(self._nb_neurones, nb_entrees + 1)  # +1 pour Biais
        

    def forward(self, entrees: np.array) -> np.array:
        """
        Passe avant dans la couche

        :param entrees: entrées de la couche, sans le neurone de biais
        :returns: une copie des sorties _zp, après les avoir mises à jour en interne

        La méthode met à jour par ailleurs :
          _z  : entrées avec le neurone de biais
          _p  : entrées pondérées
          _zp : sorties
        """
        ...

    def back(self, gk1: np.array) -> Tuple[np.array, np.array]:
        """
        Passe arrière, qui prend en paramètres les dérivées du coût par
        rapport aux sorties de la couche, et renvoie le gradient du coût
        par rapport aux poids de la couche, ainsi que les dérivées du cout
        par rapport aux entrées de la couche (id. sorties de la couche précédente),
        pour rétropropagation.

        :param gk1: vecteur des dérivées partielles du coût par rapport aux
                    sorties de la couche (vecteur de taille _nb_neurones)
        :returns: un tuple contenant
                la matrice gradient du cout par rapport aux poids (elle a la même taille que _poids)
                le vecteur contenant les dérivées du cout par rapport aux sorties de la couche précédente
                  (il contient _nb_entrees valeurs)
        """
        assert gk1.size == self._nb_neurones
        ...

    def learn(self, gradient: np.array, alpha: float):
        """
            :param gradient: matrice des dérivées du cout par rapport aux poids de la couche (telle que
                             renvoyée par back(...)
            :param alpha: facteur d'apprentissage

            La méthode met à jour les poids de la couche en utilisant la descente de gradient
        """
        ...

    @property
    def nb_entrees(self):
        """
        Propriété permettant d'accéder à l'attribut _nb_entrees
        On l'utilise ainsi :
        >>> layer.nb_entrees
        """
        return self._nb_entrees

    def __len__(self) -> int:
        """
        Renvoie le nombre de neurones de la couche. C'est une méthode magique, qui
        permet d'écrire : len(layer) pour avoir la taille de la couche
        """
        return self._nb_neurones

    def __str__(self) -> str:
        """
        Méthode magique pour afficher proprement une couche.
        À compléter selon les besoins
        """
        stri = f"""
p_k     : {self._p}
z_k+1   : {self._zp}
z_k     : {self._z}
W   :
{self._poids}
        """
        return stri

# %%

class Network:
    """
    Représente un réseau de neurones. On l'instancie en donnant le nombre d'entrées,
    puis on ajoute successivement les couches. Par exemple :

    >>> net = Network(2) # réseau à 2 entrées
    >>> net.ajoute_couche(10, sigmoid, sigmoid_prime) # ajout d'une couche cachée
    >>> net.ajoute_couche(1, sigmoid, sigmoid_prime) # couche de sortie

    """
    def __init__(self, nb_entrees):
        """
        :param nb_entrees: nombre d'entrées scalaires du réseau

        attributs internes:
          couches : la liste des couches du réseau initialisée à la liste vide
          nb_entrees : le nombre d'entrées scalaires du réseau

        """
        self.couches = []
        self.nb_entrees = nb_entrees

    def ajoute_couche(self, nb_neurones: int, fn_activation: Callable, fn_activation_prime: Callable):
        """
        :param nb_neurones: nombre de neurones de la couche
        :param fn_activation: fonction **vectorielle** d'activation
        :param fn_activation_prime: dérivée **vectorielle** de la fonction d'activation

        Instancie une nouvelle couche (en déterminant correctement le nombre d'entrées) et l'ajoute à la
        liste des couches (self.couches).
        Attention, il y a deux cas, selon que c'est la première couche ou non.
        """
        if self.couches:
            input_size = len(self.couches[-1])
        else:
            input_size = self.nb_entrees
        layer = Layer(nb_neurones, input_size, fn_activation, fn_activation_prime)
        self.couches.append(layer)

    def forward(self, entrees: np.array) -> np.array:
        """
        Passe avant dans tous le réseau pour les entrées proposées

        :param entrees: vecteur en entrée du réseau
        :returns: vecteur de sortie du réseau

        La méthode enchaîne le calcul à travers chacune des couches (méthode
        forward des couches)
        """
        ...

    def back(self, entrees: np.array, sorties_souhaitees: np.array) -> List[np.array]:
        """
        :param entrees: vecteur des entrées du réseau
        :param sorties_souhaitees: vecteur des sorties souhaitées (cible)
        :returns: Une liste contenant, pour chaque couche, le gradient du coût par rapport à poids
                  de chaque couche. Cette liste agrège les matrices renvoyées par la méthode back
                  de la classe Layer. Le premier élément de la liste contient la dérivée du coût
                  par rapport aux poids de la **première** couche.

        La méthode réalise tout d'abord une passe avant dans le réseau pour calculer la sortie
        obtenue, et tous les résultats intermédiaires sur chaque couche.
        La méthode calcule le premier vecteur contenant la dérivée du coût par rapport à la dernière
          couche (ce vecteur est calculable à partir de la dérivée de la fonction coût),
        Ce vecteur est rétropropagé (méthode Layer.back) por récupérer les dérivées du cout par rapport à
        la sortie/entrée de chacune des autres couches, ainsi que les matrices des gradients par rapport aux poids
        qui sont agrégées dans une liste avant d'être renvoyées.
        """
        sorties = self.forward(entrees)
        assert len(sorties_souhaitees) == len(self.couches[-1]) == len(sorties)
        ...


    def learn(self, gradients_moyens : List[np.array], alpha: float):
        """
        :param gradients_moyens: liste des gradients du coût par rapport aux poids, pour chacune
                                des couches. C'est éventuellement un gradient moyen, sur toute la
                                base d'apprentissage, sur un mini-batch, ou sur un seul exemple
        :param alpha: facteur d'apprentissage, retransmis aux couches

        La méthode met à jour (méthode Layer.learn()) chacune des couches du réseau
        """
        ...

    @property
    def nb_couches(self) -> int:
        """
        Propriété qui renvoie le nombre de couches actuellement
        ajoutées
        """
        return len(self.couches)


    def __str__(self) -> str:
        """
        Méthode magique qui affiche proprement un réseau, en affichant les
        paramètres de chacune des couches successivement
        """
        return "\n -------------- \n".join([str(l) for l in self.couches])

    def erreur_moyenne(self, Xs, Ys, debug=False) -> float:
        """
        Calcul de l'erreur quadratique moyenne pour plusieurs échantillons

        :param Xs: matrice des entrées, autant de lignes que d'échantillons, autant de colonnes que d'entrées
                   dans le réseau
        :param Ys: matrice des sorties souhaitées, autant de lignes que d'échantillons, autant de colonnes que
                   de sorties (nb de neurones dans la dernière couche)
        :param debug: Si à True, affiche : entrée, sortie obtenue, et sortie souhaitée pour chaque échantillon passé
        """
        E = 0
        for x, y in zip(Xs, Ys):
            yt = self.forward(np.array(x))
            E += 0.5 * np.sum((y - yt) ** 2)
            if debug:
                print(x, y, yt)
        E = E / len(Xs)
        return E

    def performance_classifieur(self, Xs: np.array, Ys:np.array) -> float:
        """
        Performance dans le cas d'un classifieur

        Dans le cas d'un classifieur, chaque neurone de sortie correspond à une classe.
        On estime que le classifieur a classifié correctement si argmax(sortie_souhaitée)
        est égal à argmax(sortie_obtenue).

        :param Xs: matrice des entrées, autant de lignes que d'échantillons, autant de colonnes que d'entrées
                   dans le réseau
        :param Ys: matrice des sorties souhaitées, autant de lignes que d'échantillons, autant de colonnes que
                   de sorties (nb de neurones dans la dernière couche)
        :returns: pourcentage d'échantillons correctement classés
        """
        ...
